martialManager.factory('authService', function($firebase) {
    var ref = new Firebase("https://testing-firebase-2121.firebaseio.com");
    return {
        ref: function () {
          return ref;
        }
    }
});

martialManager.factory('userService', function($firebaseArray) {
    var fb = new Firebase("https://testing-firebase-2121.firebaseio.com/users");
    var details = $firebaseArray(fb);
    var userService = {
        all: details,
        get: function(detailId) {
            return details.$getRecord(detailId);
        }
    };
    return userService;
});

martialManager.factory('studentService',function($firebaseArray) {
    var fb = new Firebase("https://testing-firebase-2121.firebaseio.com/students");
    var studs = $firebaseArray(fb);
    var studentService = {
        all: studs,
        get: function(studId) {
            return studs.$getRecord(studId);
        }        
    };
    return studentService;
});




         
            
          
          
          