martialManager.controller('AccountCtrl', function($scope, authService, $firebase, $location) {
        if (!$scope.user) {
            $scope.showLoginForm = true;
        }
        //Checking if user is logged in
        $scope.user = authService.ref().getAuth();
        //Login method
        $scope.login = function (em, pwd) {
            authService.ref().authWithPassword({
                email    : em,
                password : pwd
            }, function(error, authData) {
                if (error === null) {
                    $location.path("/menu/studList");
                    console.log("User ID: " + authData.uid + ", Provider: " + authData.provider);
                    console.log("Signed In");
                    $scope.user = authService.ref().getAuth();
                    $scope.$apply();
                } else {
                    console.log(error);
                }
            });
        };
        //Logout method
        $scope.logout = function () {
          authService.ref().unauth();
          $location.path("/login");
          console.log("Successfully logged out");
        };
        //Register method
        $scope.register = function(em,pwd) {
            authService.ref().createUser({
                  email: em,
                  password: pwd
                  },function(error) {
                        if (error) {
                        console.log(error);
                        } else {
                        console.log("Registration Successful: Please Sign In");
                        }
                  });
        };
});

//details controller
martialManager.controller('detailsController', function($scope, $firebase, authService, $firebaseArray,$location, userService){
    $scope.user = authService.ref().getAuth();

    $scope.submitUserDetails = function() {
        $scope.newDetails = userService.all;
            $scope.newDetails.$add({
                by: $scope.user.password.email,
                clubName: $scope.clubName || "Not Applicable",
                mainLocation:  $scope.mainLocation || "Not Applicable",
                address: $scope.address || "Not Applicable",
                contactPhone: $scope.contactPhone || "Not Applicable",
                contactPerson: $scope.contactPerson || "Not Applicable",
                website: $scope.website || "Not Applicable"
            });
        console.log("Details Added");
        $location.path("/menu/account")
    }
});

martialManager.controller('detailListController',function($scope, authService, userService){
    $scope.user = authService.ref().getAuth();
    $scope.details = userService.all;
});

martialManager.controller('editDetailsController',function($scope, authService, userService){
    $scope.user = authService.ref().getAuth();
    $scope.editDetails = userService.all;
});

//add controller
martialManager.controller('addStudentController',function($scope, $firebase, authService, $firebaseArray,$state,studentService){
    $scope.user = authService.ref().getAuth();
    
    $scope.submitStudent = function(){
        $scope.newStud = studentService.all;
        
        if ($scope.adult == 'yes' && $scope.studEndDate == undefined) {
            $scope.newStud.$add({
                by: $scope.user.password.email,
                studentFirstName: $scope.studFirstName,
                studentLastName:  $scope.studLastName,
                studentAddress: $scope.studAddress,
                studentEmail: $scope.studEmail,
                studentPhone: $scope.studPhone,
                studentDob: $scope.studDob.toString(),
                studentStartDate: $scope.studStartDate.toString(),
                studentEndDate: "Not Applicable",
                studentPaymentType: $scope.studPaymentType,
                adult: $scope.adult,
                emergencyName: $scope.emergName,
                emergencyPhone: $scope.emergPhone,
            });
        }
        else if ($scope.adult == 'yes' && $scope.studEndDate != undefined) {
                $scope.newStud.$add({
                by: $scope.user.password.email,
                studentFirstName: $scope.studFirstName,
                studentLastName:  $scope.studLastName,
                studentDob: $scope.studDob.toString(),
                studentStartDate: $scope.studStartDate.toString(),
                studentEndDate: $scope.studEndDate.toString(),
                studentAddress: $scope.studAddress,
                studentEmail: $scope.studEmail,
                studentPhone: $scope.studPhone,
                adult: $scope.adult,
                emergencyName: $scope.emergName,
                emergencyPhone: $scope.emergPhone,
            });
        }
        else if ($scope.adult == 'no' && $scope.studEndDate == undefined) {
            $scope.newStud.$add({
                by: $scope.user.password.email,
                studentFirstName: $scope.studFirstName,
                studentLastName:  $scope.studLastName,
                studentDob: $scope.studDob.toString(),
                adult: $scope.adult,
                studentStartDate: $scope.studStartDate.toString(),
                studentEndDate: "Not Applicable",
                parentFirstName: $scope.parFirstName,
                parentLastName: $scope.parLastName,
                parentAddress: $scope.parAddress,
                parentEmail: $scope.parEmail,
                studentAddress: $scope.studAddress || "Not Applicable",
                studentEmail: $scope.studEmail || "Not Applicable",
                studentPhone: $scope.studPhone || "Not Applicable"
        });
        }
        else if ($scope.adult == 'no' && $scope.studEndDate != undefined) {
            $scope.newStud.$add({
                by: $scope.user.password.email,
                studentFirstName: $scope.studFirstName,
                studentLastName:  $scope.studLastName,
                studentDob: $scope.studDob.toString(),
                adult: $scope.adult,
                studentStartDate: $scope.studStartDate.toString(),
                studentEndDate: $scope.studEndDate.toString(),
                parentFirstName: $scope.parFirstName,
                parentLastName: $scope.parLastName,
                parentAddress: $scope.parAddress,
                parentEmail: $scope.parEmail,
                studentAddress: $scope.studAddress || "Not Applicable",
                studentEmail: $scope.studEmail || "Not Applicable",
                studentPhone: $scope.studPhone || "Not Applicable"
        });
        }

        $state.go('menu.studList');
    };
});



martialManager.controller('listController',function($scope, authService, studentService){
    $scope.user = authService.ref().getAuth();
    $scope.students = studentService.all;
});
 

martialManager.controller('studentController',function($scope, authService, studentService,$stateParams,$state){
    $scope.user = authService.ref().getAuth();
    $scope.singleStudent = studentService.get($stateParams.id);
});





martialManager.controller('deleteController',function($scope, authService, studentService,$state,$firebaseArray,$ionicActionSheet){
    $scope.user = authService.ref().getAuth();
    $scope.studs = studentService.all;
    
    $scope.showDetails = function(id) {
        $ionicActionSheet.show({
            destructiveText: 'Delete',
            titleText: 'Sure you want to delete?',
            cancelText: 'Cancel',
            destructiveButtonClicked: function() {
                var rem = $scope.studs.$getRecord(id);
                $scope.studs.$remove(rem);
                return true;
            }
        });
    };
});


martialManager.controller('editController',function($scope, authService, studentService){
    $scope.user = authService.ref().getAuth();
    $scope.editStudents = studentService.all;
});


martialManager.controller('studentEditController',function($scope, authService, studentService, $stateParams,$state){
    $scope.user = authService.ref().getAuth();
    $scope.allStuds = studentService.all;
    $scope.singleStudent = studentService.get($stateParams.id);
    $scope.title = $scope.singleStudent.studentFirstName;
    $scope.studentLastName =  $scope.singleStudent.studentLastName;
    $scope.studentAddress = $scope.singleStudent.studentAddress;
    $scope.studentEmail = $scope.singleStudent.studentEmail;
    $scope.studentPhone = $scope.singleStudent.studentPhone;
    $scope.studentDob = $scope.singleStudent.studentDob;
    $scope.studentStartDate = $scope.singleStudent.studentStartDate;
    $scope.studentPaymentType = $scope.singleStudent.studentPaymentType;
    $scope.adult = $scope.singleStudent.adult;
    $scope.emergencyName = $scope.singleStudent.emergencyName;
    $scope.emergencyPhone = $scope.singleStudent.emergencyPhone;
    $scope.parentFirstName = $scope.singleStudent.parentFirstName;
    $scope.parentLastName = $scope.singleStudent.parentLastName;
    $scope.parentAddress = $scope.singleStudent.parentAddress;
    $scope.parentEmail = $scope.singleStudent.parentEmail
    $scope.myid = $scope.singleStudent.$id;
    $scope.updateStudent = function(id) {
        if ($scope.adult == 'yes' && $scope.studentEndDate == undefined) {
            var ed = $scope.allStuds.$getRecord(id);
            ed.studentFirstName = $scope.title;
            ed.studentLastName = $scope.studentLastName;
            ed.studentAddress = $scope.studentAddress;
            ed.studentEmail = $scope.studentEmail;
            ed.studentPhone = $scope.studentPhone;
            ed.studentDob = $scope.studentDob.toString();
            ed.studentStartDate = $scope.studentStartDate.toString();
            ed.studentEndDate = "Not Applicable";
            ed.studentPaymentType = $scope.studentPaymentType;
            ed.adult = $scope.adult;
            ed.emergencyName = $scope.emergencyName;
            ed.emergencyPhone = $scope.emergencyPhone;
        }
        else if ($scope.adult == 'yes' && $scope.studentEndDate != undefined) {
            var ed = $scope.allStuds.$getRecord(id);
            ed.studentFirstName = $scope.title;
            ed.studentLastName = $scope.studentLastName;
            ed.studentAddress = $scope.studentAddress;
            ed.studentEmail = $scope.studentEmail;
            ed.studentPhone = $scope.studentPhone;
            ed.studentDob = $scope.studentDob.toString();
            ed.studentStartDate = $scope.studentStartDate.toString();
            ed.studentEndDate = $scope.studentEndDate.toString();
            ed.studentPaymentType = $scope.studentPaymentType;
            ed.adult = $scope.adult;
            ed.emergencyName = $scope.emergencyName;
            ed.emergencyPhone = $scope.emergencyPhone;
        }
        else if ($scope.adult == 'no' && $scope.studentEndDate == undefined) {
            var ed = $scope.allStuds.$getRecord(id);
            ed.studentDob = $scope.studentDob.toString();
            ed.studentStartDate = $scope.studentStartDate.toString();
            ed.studentEndDate = $scope.studentEndDate.toString();
            ed.studentPaymentType = $scope.studentPaymentType;
            ed.adult = $scope.adult;
            ed.studentFirstName = $scope.title;
            ed.studentLastName = $scope.studentLastName;
            ed.parentFirstName = $scope.parentFirstName;
            ed.parentLastName = $scope.parentLastName;
            ed.parentAddress = $scope.parentAddress;
            ed.parentEmail = $scope.parentEmail;
            ed.studentAddress = $scope.studentAddress || "Not Applicable";
            ed.studentEmail = $scope.studentEmail || "Not Applicable";
            ed.studentPhone = $scope.studentPhone || "Not Applicable";
        }
        else if ($scope.adult == 'no' && $scope.studentEndDate != undefined) {
            var ed = $scope.allStuds.$getRecord(id);
            ed.studentDob = $scope.studentDob.toString();
            ed.studentStartDate = $scope.studentStartDate.toString();
            ed.studentEndDate = "Not Applicable";
            ed.studentPaymentType = $scope.studentPaymentType;
            ed.adult = $scope.adult;
            ed.studentFirstName = $scope.title;
            ed.studentLastName = $scope.studentLastName;
            ed.parentFirstName = $scope.parentFirstName;
            ed.parentLastName = $scope.parentLastName;
            ed.parentAddress = $scope.parentAddress;
            ed.parentEmail = $scope.parentEmail;
            ed.studentAddress = $scope.studentAddress || "Not Applicable";
            ed.studentEmail = $scope.studentEmail || "Not Applicable";
            ed.studentPhone = $scope.studentPhone || "Not Applicable";
        }

        $scope.allStuds.$save(ed);
        $state.go('menu.edit');
    };
});

martialManager.controller('detailEditController',function($scope,authService, userService,$stateParams,$state){
    $scope.user = authService.ref().getAuth();
    $scope.allDetails = userService.all;
    $scope.singleDetail = userService.get($stateParams.id);
    $scope.title = $scope.singleDetail.clubName;
    $scope.mainLocation = $scope.singleDetail.mainLocation;
    $scope.address = $scope.singleDetail.address;
    $scope.contactPerson = $scope.singleDetail.contactPerson;
    $scope.contactPhone = $scope.singleDetail.contactPhone;
    $scope.website = $scope.singleDetail.website;
    $scope.myid = $scope.singleDetail.$id;
    $scope.updateDetail = function(id) {
        var ed = $scope.allDetails.$getRecord(id);
        ed.clubName = $scope.title || "Not Applicable";
        ed.mainLocation = $scope.mainLocation || "Not Applicable";
        ed.address = $scope.address || "Not Applicable";
        ed.contactPerson = $scope.contactPerson || "Not Applicable";
        ed.contactPhone = $scope.contactPhone || "Not Applicable";
        ed.website = $scope.website || "Not Applicable";
        $scope.allDetails.$save(ed);
        $state.go('menu.editUserDetails');
    };
});

